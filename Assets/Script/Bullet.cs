﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float moveSpeed = 5f;
    Rigidbody2D rb;
    Player target;
    Vector2 moveDirection;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        target = GameObject.FindObjectOfType<Player>();
        moveDirection = (target.transform.position - transform.position).normalized * moveSpeed;
        rb.velocity = new Vector2(moveDirection.x, moveDirection.y);
        Destroy(gameObject, 3f);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.transform.tag == "Player")
        {
            collision.SendMessage("EnemyKnockBack", transform.position.x);
            GameObject.Find("GameManager").GetComponent<GameManager>().decreaseLife("Player");
            Destroy(gameObject);
        }
        else if (collision.transform.tag == "Suelo")
        {
            Destroy(gameObject);
        }

    }

}