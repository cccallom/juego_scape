﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AllUnits : MonoBehaviour
{
    public GameObject[] units;
    public GameObject prefab;
    public int numItem = 7;
    public Vector3 range = new Vector3(3, 3, 3);

    
    // Start is called before the first frame update
    void Start()
    {
        units = new GameObject[numItem];
        Debug.Log(units.Length);
        for(int i = 0; i < numItem; i++)
        {
            //Debug.Log("creando instancia");
            Vector3 unitPos = new Vector3(Random.Range(-range.x, range.x),
                Random.Range(-range.y, range.y), Random.Range(0,0));
            //Debug.Log("vector" + unitPos);
            units[i] = Instantiate(prefab, transform.position + unitPos, Quaternion.identity);
        }
    }
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireCube(this.transform.position, range * 2);
        Gizmos.color = Color.green;
        Gizmos.DrawSphere(this.transform.position, 0.2f);
    }
    
}
