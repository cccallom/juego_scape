﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTo : MonoBehaviour
{
    public Transform goal;
    public float SpaceBetween = 1.5f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Vector2.Distance(goal.position, transform.position)>= SpaceBetween)
        {
            Vector2 direction = goal.position - transform.position;
            transform.Translate(direction * Time.deltaTime);
        }
    }
}
