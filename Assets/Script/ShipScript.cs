﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class ShipScript : MonoBehaviour
{

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            if (GameObject.Find("GameManager").GetComponent<GameManager>().isAllItemsColleted())
            {
                GameObject.Find("Canvas").transform.GetChild(2).gameObject.SetActive(true);
                GameObject.Find("MensajeAdvertencia").GetComponent<Text>().text = "Items Conseguidos!! Ahora podemos escapar\n Bien hecho, Juego Terminado";
                Invoke("regresarMenu", 2f);
            }
            else
            {
                GameObject.Find("Canvas").transform.GetChild(2).gameObject.SetActive(true);
                GameObject.Find("MensajeAdvertencia").GetComponent<Text>().text = "Aun te faltan conseguir algunos items, revisa tu inventario!!";
            }
        }
        if (collision.gameObject.tag == "Enemy")
        {
            GameObject.Find("GameManager").GetComponent<GameManager>().decreaseLife("Ship");
            if(collision.gameObject.GetComponent<Enemy>())
                collision.gameObject.GetComponent<Enemy>().takeDamage();
        }
    }

    void regresarMenu()
    {
        SceneManager.LoadScene(0);
    }
}
