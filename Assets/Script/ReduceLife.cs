﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReduceLife : MonoBehaviour
{
    private GameObject barraLife;
    private int life;
    public int totalLife;
    private int topLife;
    RectTransform rect;

    public void Start()
    {
        rect = GetComponent<RectTransform>();
        topLife = totalLife;
        life = totalLife;
        totalLife = 80 / totalLife;
        
    }

    public void reduceLife(string tag)
    {
        if (life > 0)
        {
            rect.position = new Vector3(rect.position.x - totalLife, rect.position.y, rect.position.z);
            rect.sizeDelta = new Vector2(rect.sizeDelta.x - totalLife*2, rect.sizeDelta.y);
            life = life - 1;
        }
        else
        {

        }
    }

    public void increaseLife(string tag)
    {
        if (life <= topLife)
        {
            rect.position = new Vector3(rect.position.x + totalLife, rect.position.y, rect.position.z);
            rect.sizeDelta = new Vector2(rect.sizeDelta.x + totalLife * 2, rect.sizeDelta.y);
            life = life + 1;
        }
        else
        {

        }
    }
}
