﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerGame : MonoBehaviour
{
    public Text crono;
    public int minsStart=4;
    int mins;
    float segs;
    float currentTime;
    // Start is called before the first frame update
    void Start()
    {
        currentTime = 59;
        mins = minsStart - 1;
        
    }

    // Update is called once per frame
    void Update()
    {
        currentTime -= 1 * Time.deltaTime;
        crono.text = mins + ":" + currentTime.ToString("0");
        if (currentTime <= 0)
        {
            mins = mins - 1;
            currentTime = 59;
            if (mins <= 0)
            {
                mins = 0;
                currentTime = 0;
            }
        }
    }
}
