﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeparateTo : MonoBehaviour
{
    GameObject[] IA;
    public float SpaceBetween = 2.5f;
    // Start is called before the first frame update
    void Start()
    {
        IA = GameObject.FindGameObjectsWithTag("IA");
    }

    // Update is called once per frame
    void Update()
    {
        foreach(GameObject obj in IA)
        {
            if(obj != gameObject)
            {
                float distance = Vector2.Distance(obj.transform.position, this.transform.position);
                if(distance<= SpaceBetween)
                {
                    Vector2 direction = transform.position - obj.transform.position;
                    transform.Translate(direction * Time.deltaTime);
                }
            }
        }
    }
}
