﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour
{
    public float maxLeft=-14, maxRight=14;
    public float maxTop=50, maxBot= -8;
    public GameObject player;
    public float smoothTime;
    private Vector2 velocity;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void FixedUpdate()
    {
        float posX = Mathf.SmoothDamp(transform.position.x, player.transform.position.x, 
            ref velocity.x, smoothTime);
        float posY = Mathf.SmoothDamp(transform.position.y, player.transform.position.y, 
            ref velocity.y, smoothTime);
        transform.position = new Vector3(Mathf.Clamp( posX,maxLeft,maxRight),
            Mathf.Clamp( posY,maxBot,maxTop), 
            transform.position.z);
    }
}
