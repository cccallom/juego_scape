﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public Vector3 startPosition;
    public int minsStart = 4;
    int mins;
    float segs;
    float currentTime;
    public Vector3 spawnEnemy;
    public int lifePlayer;
    int topLifePlayer;
    public int totalLifePlayer;
    RectTransform barLifePlayer;
    public int lifeShip;
    int topLifeShip;
    public int totalLifeShip;
    RectTransform barLifeShip;

    private int item01;
    public int neededItem01;
    private int item02;
    public int neededItem02;
    private int item03;
    public int neededItem03;
    private int item04;
    public int neededItem04;

    private Text crono;

    float iniBarLifeShip;
    float iniBarLifePlayer;

    bool start=false;
    public GameObject enemigo;


    private void Start()
    {
        item01 = 0;
        item02 = 0;
        item03 = 0;
        item04 = 0;
        spawnEnemy = new Vector3(3, 4, 0);
        startPosition = new Vector3(-7,0,0);
        currentTime = 59;
        mins = minsStart - 1;
        

        topLifePlayer = totalLifePlayer;
        lifePlayer = totalLifePlayer;
        totalLifePlayer = 80 / totalLifePlayer;

        topLifeShip = totalLifeShip;
        lifeShip = totalLifeShip;
        totalLifeShip = 80 / totalLifeShip;

        iniBarLifePlayer = GameObject.Find("BarLifePlayer").GetComponent<RectTransform>().position.x;
        iniBarLifeShip = GameObject.Find("BarLifeShip").GetComponent<RectTransform>().position.x;

        GameObject.Find("Canvas").transform.GetChild(2).gameObject.SetActive(true);
        GameObject.Find("MensajeAdvertencia").GetComponent<Text>().text = "Empieza tu busqueda!!!\n" +
            "Busca los minerales que necesitas y regresa a tu nave!\n" +
            "Ten cuidado con tus enemigos";

    }
    private void Update()
    {
        counterTime();
        actualizarBarraVida();
        actualizarItems();
        checkGameOver();
        if (GameObject.FindObjectOfType<Player>().gameObject.transform.position.y <= -15)
        {
            lifePlayer = 0;
        }
    }
    IEnumerator Spawn(int i)
    {
        yield return new WaitForSeconds(i);
        GameObject.Instantiate(enemigo, spawnEnemy, Quaternion.identity);
    }
    void spawnEnemies()
    {
        for(int i = 0; i<10; i++)
        {
            StartCoroutine(Spawn(i));
        }
    }

    private void actualizarBarraVida()
    {
        barLifeShip = GameObject.Find("BarLifeShip").GetComponent<RectTransform>();
        barLifePlayer = GameObject.Find("BarLifePlayer").GetComponent<RectTransform>();

        barLifePlayer.sizeDelta = new Vector2(3.9f+totalLifePlayer*2*lifePlayer,
            barLifePlayer.sizeDelta.y);
        barLifePlayer.position = new Vector3(iniBarLifePlayer + totalLifePlayer * lifePlayer,
            barLifePlayer.position.y,
            barLifePlayer.position.z);

        barLifeShip.sizeDelta = new Vector2(1.19f + totalLifeShip * 2 * lifeShip, 
            barLifeShip.sizeDelta.y);
        barLifeShip.position = new Vector3(iniBarLifeShip+ totalLifeShip*lifeShip, 
            barLifeShip.position.y, 
            barLifeShip.position.z);
    }

    public void increaseLife(string tag)
    {
        if (tag == "Player")
        {
            if(lifePlayer<topLifePlayer)
                lifePlayer = lifePlayer + 1;
        }
        if (tag == "Ship")
        {
            if(lifeShip<topLifeShip)
                lifeShip = lifeShip + 1;
        }
    }

    public void decreaseLife(string tag)
    {
        if (tag == "Player")
        {
            if(lifePlayer>0)
                lifePlayer = lifePlayer - 1;
        }
        if (tag == "Ship")
        {
            if(lifeShip>0)
                lifeShip = lifeShip - 1;
        }
    }



    private void Awake()
    {
        MakeSingleton();
    }
    private void MakeSingleton()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    public void counterTime()
    {
        crono = GameObject.Find("CronometroTimer").GetComponent<Text>();
        currentTime -= 1 * Time.deltaTime;
        crono.text = mins + ":" + currentTime.ToString("0");
        if (currentTime <= 0)
        {
            mins = mins - 1;
            currentTime = 59;
            if (mins <= 0)
            {
                Invoke("spawnEnemies", 10f);
                startPosition = new Vector3(7.3f, 0.2f, 0);
                GameObject.Find("Canvas").transform.GetChild(2).gameObject.SetActive(true);
                GameObject.Find("MensajeAdvertencia").GetComponent<Text>().text = "El tiempo se acabo, seras transportado cerca de la nave\n" +
                    "Se avecinan enemigos!!";
                Invoke("changeScene", 2);
                mins = minsStart-1;
                currentTime = 59;
            }
        }
    }

    private void actualizarItems()
    {
        GameObject it01 = GameObject.Find("MinFound01");
        GameObject it02 = GameObject.Find("MinFound02");
        GameObject it03 = GameObject.Find("MinFound03");
        GameObject it04 = GameObject.Find("MinFound04");
        GameObject item01Rest = GameObject.Find("MinRest01");
        GameObject item02Rest = GameObject.Find("MinRest02");
        GameObject item03Rest = GameObject.Find("MinRest03");
        GameObject item04Rest = GameObject.Find("MinRest04");

        it01.GetComponent<Text>().text = item01 + "";
        it02.GetComponent<Text>().text = item02 + "";
        it03.GetComponent<Text>().text = item03 + "";
        it04.GetComponent<Text>().text = item04 + "";

        item01Rest.GetComponent<Text>().text = (neededItem01 - item01) + "";
        item02Rest.GetComponent<Text>().text = (neededItem02 - item02) + "";
        item03Rest.GetComponent<Text>().text = (neededItem03 - item03) + "";
        item04Rest.GetComponent<Text>().text = (neededItem04 - item04) + "";
        
    }

    public void addItem(int itemType)
    {
        if (itemType == 1)
        {
            item01++;
        }
        else if (itemType == 2)
        {
            item02++;
        }
        else if (itemType == 3)
        {
            item03++;   
        }
        else if (itemType == 4)
        {
            item04++;
        }
        //actualizarItems();
    }

    public bool isAllItemsColleted()
    {
        if((item01>= neededItem01)&& (item02 >= neededItem02)&& (item03 >= neededItem03) && (item04>= neededItem04))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void checkGameOver()
    {
        if(lifePlayer<=0 || lifeShip <= 0)
        {
            GameObject.Find("Canvas").transform.GetChild(2).gameObject.SetActive(true);
            GameObject.Find("MensajeAdvertencia").GetComponent<Text>().text = "Has perdido, intentalo la proxima vez!!!";
            Invoke("regresarMenu", 3f);
        }
    }
    void regresarMenu()
    {
        SceneManager.LoadScene(0);
    }

    void changeScene()
    {
        SceneManager.LoadScene(1);
    }
}

