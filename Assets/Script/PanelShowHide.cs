﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelShowHide : MonoBehaviour
{
    public GameObject panel;
    // Start is called before the first frame update
    public void openPanel()
    {
        Animator anim = panel.GetComponent<Animator>();
        if(anim != null)
        {
            bool isOpen = anim.GetBool("open");
            anim.SetBool("open", !isOpen);
        }
    }
}
