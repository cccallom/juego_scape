﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;
public class EnemyIAScript : MonoBehaviour
{
    public AIPath aiPath;
    Vector2 direction;
    private Rigidbody2D rigidbody2D;
    private SpriteRenderer sprite;
    public float enemyHealt=2;
    // Start is called before the first frame update
    void Start()
    {
        rigidbody2D = GetComponent<Rigidbody2D>();
        sprite = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        checkHealth();
        //faceVelocity();
    }
    void faceVelocity()
    {
        direction = aiPath.desiredVelocity;
        transform.right = direction;
    }

    /*
    private void FixedUpdate()
    {
        rigidbody2D.AddForce(Vector2.right * speed);
        float limitSpeed = Mathf.Clamp(rigidbody2D.velocity.x, -maxSpeed, maxSpeed);
        rigidbody2D.velocity = new Vector2(limitSpeed, rigidbody2D.velocity.y);

        if(rigidbody2D.velocity.x>-0.01f && rigidbody2D.velocity.x < 0.01f)
        {
            speed = -speed;
            rigidbody2D.velocity     = new Vector2(speed, rigidbody2D.velocity.y);
        }
        if (speed < 0)
            sprite.flipX = true;
        if (speed > 0f)
            sprite.flipX = false;
    } 
    */

    public void takeDamage()
    {
        enemyHealt -= 1;
        Player target = GameObject.FindObjectOfType<Player>();
        float side = Mathf.Sign(target.gameObject.transform.position.x - transform.position.x);
        rigidbody2D.AddForce(Vector2.left * side, ForceMode2D.Impulse);
        Invoke("EnableMov", 0.5f);
        sprite.color = Color.red;
    }

    void EnableMov()
    {
        sprite.color = Color.white;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            float yOff = 0.7f;
            
            if (transform.position.y + yOff < collision.transform.position.y)
            {
                Destroy(gameObject);
                collision.SendMessage("enemyJump");
            }
            else
            {
                collision.SendMessage("EnemyKnockBack", transform.position.x);
                GameObject.Find("GameManager").GetComponent<GameManager>().decreaseLife("Player");
            }
        }
        
    }

    public void checkHealth()
    {
        if (enemyHealt <= 0)
        {
            Destroy(gameObject);
        }
    }
}
