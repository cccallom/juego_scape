﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallenPlatform : MonoBehaviour
{
    public float fallDelay=1f;
    public float respawnDelay = 5f;
    private Rigidbody2D rgbd2D;
    private BoxCollider2D bc2D;
    private Vector3 start;

    // Start is called before the first frame update
    void Start()
    {
        rgbd2D = GetComponent<Rigidbody2D>();
        bc2D = GetComponent<BoxCollider2D>();
        start = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Invoke("fall", fallDelay);
            Invoke("respawn", fallDelay + respawnDelay);
        }
    }

    private void fall()
    {
        rgbd2D.isKinematic = false;
        bc2D.isTrigger = true;
    }

    private void respawn()
    {
        transform.position = start;
        rgbd2D.isKinematic = true;
        rgbd2D.velocity = Vector3.zero;
        bc2D.isTrigger = false;
    }
}
