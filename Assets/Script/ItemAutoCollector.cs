﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemAutoCollector : MonoBehaviour
{
    public Transform player;
    public float vel;
    public float radio =2f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void FixedUpdate()
    {
        if (Mathf.Abs(player.position.x - transform.position.x) < radio)
        {
            float fixedUpdate = vel * Time.deltaTime;
            transform.position = Vector3.MoveTowards(transform.position, player.position, vel * fixedUpdate);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            float off = 0.7f;
            if((transform.position.x + off > collision.transform.position.x)
                && (transform.position.y + off > collision.transform.position.y))
                Destroy(gameObject);
        }
    }
}
