﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyIAFlock : MonoBehaviour
{
    private Rigidbody2D rigidbody2D;
    private SpriteRenderer sprite;
    public float enemyHealt = 2;

    public GameObject manager;
    public Vector2 location = Vector2.zero;
    public Vector2 velocity;
    public Vector2 goalPos = Vector2.zero;
    Vector2 currentForce;





    void Start()
    {
        rigidbody2D = GetComponent<Rigidbody2D>();
        sprite = GetComponent<SpriteRenderer>();

        velocity = new Vector2(Random.Range(0.01f, 0.1f), Random.Range(0.01f, 0.1f));
        location = new Vector2(gameObject.transform.position.x, gameObject.transform.position.y);

    }

    Vector2 seek(Vector2 target)
    {
        return (target - location);
    }

    void applyForce(Vector2 f)
    {
        Vector3 force = new Vector3(f.x, f.y, 0);
        GetComponent<Rigidbody2D>().AddForce(force);
        Debug.DrawRay(transform.position, force, Color.white);
    }

    void flock()
    {
        location = transform.position;
        velocity = GetComponent<Rigidbody2D>().velocity;
        Vector2 gl;
        gl = seek(goalPos);
        currentForce = gl;
        currentForce = currentForce.normalized;
        applyForce(currentForce);

    }



    // Update is called once per frame
    void Update()
    {
        flock();
        goalPos = manager.transform.position;
        checkHealth();
        //faceVelocity();
    }
    void faceVelocity()
    {

    }


    public void takeDamage()
    {
        enemyHealt -= 1;
        Player target = GameObject.FindObjectOfType<Player>();
        float side = Mathf.Sign(target.gameObject.transform.position.x - transform.position.x);
        rigidbody2D.AddForce(Vector2.left * side, ForceMode2D.Impulse);
        Invoke("EnableMov", 0.5f);
        sprite.color = Color.red;
    }

    void EnableMov()
    {
        sprite.color = Color.white;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            float yOff = 0.7f;

            if (transform.position.y + yOff < collision.transform.position.y)
            {
                Destroy(gameObject);
                collision.SendMessage("enemyJump");
            }
            else
            {
                collision.SendMessage("EnemyKnockBack", transform.position.x);
                GameObject.Find("GameManager").GetComponent<GameManager>().decreaseLife("Player");
            }
        }

    }

    public void checkHealth()
    {
        if (enemyHealt <= 0)
        {
            Destroy(gameObject);
        }
    }
}

