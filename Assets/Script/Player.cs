﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    public Vector3 inicio;
    public float maxSpeed = 5f;
    public float speed = 15f;
    private Rigidbody2D rigidbody2D;
    private SpriteRenderer sprite;
    private Animator anim;
    public bool grounded;
    public float jumpPower = 6.5f;
    public bool jump;
    private bool dobleJump;
    private bool movement = true;
    public Transform attackPos;
    public float attackRange;
    public LayerMask whatEnemies;
    public float timeEntreAtaque;
    public float startTimeEntreAtaque;
    // Start is called before the first frame update
    void Start()
    {
        transform.position = GameObject.Find("GameManager").GetComponent<GameManager>().startPosition;
        rigidbody2D = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        sprite = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (timeEntreAtaque <= 0)
        {
            if (Input.GetKey(KeyCode.K))
            {
                anim.Play("Player-Atack");
                Collider2D[] enemiesToDamage = Physics2D.OverlapCircleAll(attackPos.position, attackRange, whatEnemies);
                for(int i = 0; i < enemiesToDamage.Length; i++)
                {
                    if (enemiesToDamage[i].GetComponent<Enemy>())
                    {
                        enemiesToDamage[i].GetComponent<Enemy>().takeDamage();
                    }
                    else if (enemiesToDamage[i].GetComponent<EnemyRange>())
                    {
                        enemiesToDamage[i].GetComponent<EnemyRange>().takeDamage();
                    }
                    else if(enemiesToDamage[i].GetComponent<EnemyIAScript>())
                    {
                        enemiesToDamage[i].GetComponent<EnemyIAScript>().takeDamage();
                    }
                }
            }
            timeEntreAtaque = startTimeEntreAtaque;
        }
        else
        {
            timeEntreAtaque -= Time.deltaTime; 
        }


        anim.SetFloat("speed", Mathf.Abs(rigidbody2D.velocity.x));
        anim.SetBool("grounded", grounded);
        if (grounded)
        {
            dobleJump = true;
        }
        if ((Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.UpArrow)))
        {
            if (grounded)
            {
                dobleJump = true;
                jump = true;
            }
            else if (dobleJump)
            {
                jump = true;
                dobleJump = false;
            }
        }
        
    }

    private void FixedUpdate()
    {
        Vector3 fixedVelocity = rigidbody2D.velocity;
        fixedVelocity.x *= 0.75f;

        if (grounded)
            rigidbody2D.velocity = fixedVelocity;
        float x = Input.GetAxis("Horizontal");
        if (!movement) x = 0;
        rigidbody2D.AddForce(Vector2.right * speed * x);
        float limitSpeed = Mathf.Clamp(rigidbody2D.velocity.x, -maxSpeed, maxSpeed);
        rigidbody2D.velocity = new Vector2(limitSpeed, rigidbody2D.velocity.y);
        if (x < 0)
            sprite.flipX = true;
        //transform.localScale = new Vector3(-1, 1, 1);
        if (x > 0.1f)
            sprite.flipX = false;
            //transform.localScale = new Vector3(1, 1, 1);
        if (jump)
        {
            rigidbody2D.velocity = new Vector2(rigidbody2D.velocity.x, 0);
            rigidbody2D.AddForce(Vector2.up * jumpPower, ForceMode2D.Impulse);
            jump = false;
        }

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "PlatformMovil")
        {
            rigidbody2D.velocity = new Vector3(0f, 0f, 0f);
            transform.parent = collision.transform;
            grounded = true;
        }
 
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Portal")
        {
            GameObject.Find("GameManager").GetComponent<GameManager>().startPosition = collision.gameObject.GetComponent<Portal>().getUbication();
            SceneManager.LoadScene(collision.gameObject.GetComponent<Portal>().getScenePortal());
            Debug.Log("enter");
        }
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Suelo")
            grounded = true;
        if (collision.gameObject.tag == "PlatformMovil")
        {
            transform.parent = collision.transform;
            grounded = true;
        }
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Suelo")
            grounded = false;
        if (collision.gameObject.tag == "PlatformMovil")
        {
            transform.parent = null;
            grounded = false;
        }
    }


    public void enemyJump()
    {
        jump = true;
    }

    public void EnemyKnockBack(float enemyPosX)
    {
        jump = true;
        float side = Mathf.Sign(enemyPosX - transform.position.x);
        rigidbody2D.AddForce(Vector2.left * side * jumpPower, ForceMode2D.Impulse);
        movement = false;
        Invoke("EnableMovement", 0.5f);
        sprite.color = Color.red;
    }

    void EnableMovement()
    {
        movement = true;
        sprite.color = Color.white;
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(attackPos.position, attackRange);
    }
}
