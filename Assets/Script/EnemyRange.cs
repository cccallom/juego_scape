﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyRange : MonoBehaviour
{
    public float maxSpeed = 1f;
    public float speed = 1f, shootSpeed;
    public float distancePlayer = 4f;
    public float rangeAtack = 7f;
    private Rigidbody2D rigidbody2D;
    private SpriteRenderer sprite;
    public Transform player, shootPos;
    private Animator anim;
    private bool needFlip = false;
    public float timeShots;
    public GameObject bullet;
    public int enemyHealt = 2;


    // Start is called before the first frame update
    void Start()
    {
        rigidbody2D = GetComponent<Rigidbody2D>();
        sprite = GetComponent<SpriteRenderer>();
        anim = GetComponent<Animator>();

    }

    private void Update()
    {
        checkHealth();
        float distance = Mathf.Abs(transform.position.x - player.position.x);
        anim.SetFloat("speed", Mathf.Abs(rigidbody2D.velocity.x));


    }

    private void FixedUpdate()
    {
        float distancia = Mathf.Abs(transform.position.x - player.position.x);
        float dir = player.position.x - transform.position.x;


        if (distancia < rangeAtack && distancia > distancePlayer)
        {
            if (needFlip)
            {
                transform.localScale = new Vector3(transform.localScale.x*-1, 1, 1);
                needFlip = false;
            }
            timeShots += Time.deltaTime;
            if (timeShots > 2)
            {
                Instantiate(bullet, shootPos.position, Quaternion.identity);
                anim.Play("Enemy_Range-Atack");
                timeShots = 0;
            }

        }

        else if (distancia < distancePlayer)
        {
            if (dir > 0)
            {
                if (speed > 0)
                {
                    speed = -speed;
                }
            }
            else
            {
                if (speed < 0)
                {
                    speed = -speed;
                }
            }


            rigidbody2D.AddForce(Vector2.right * speed);
            float limitSpeed = Mathf.Clamp(rigidbody2D.velocity.x, -maxSpeed, maxSpeed);
            rigidbody2D.velocity = new Vector2(limitSpeed, rigidbody2D.velocity.y);
            if (rigidbody2D.velocity.x > 0)
            {
                transform.localScale = new Vector3(-1, 1, 1);
                needFlip = true;
            }

            if (rigidbody2D.velocity.x < 0f)
            {
                transform.localScale = new Vector3(1, 1, 1);
                needFlip = true;
            }
        }

        else
        {
            rigidbody2D.velocity = new Vector2(0, 0);
        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            float yOff = 0.7f;
            if (transform.position.y + yOff < collision.transform.position.y)
            {
                Destroy(gameObject);
                collision.SendMessage("enemyJump");
            }
            else
            {
                collision.SendMessage("EnemyKnockBack", transform.position.x);
                GameObject.Find("GameManager").GetComponent<GameManager>().decreaseLife("Player");
            }
        }
    }

    public void takeDamage()
    {
        enemyHealt -= 1;
        Player target = GameObject.FindObjectOfType<Player>();
        float side = Mathf.Sign(target.gameObject.transform.position.x - transform.position.x);
        rigidbody2D.AddForce(Vector2.left * side, ForceMode2D.Impulse);
        Invoke("EnableMov", 0.5f);
        sprite.color = Color.red;
    }

    void EnableMov()
    {
        sprite.color = Color.white;
    }

    public void checkHealth()
    {
        if (enemyHealt == 0)
        {
            Destroy(gameObject);
        }
    }
}

